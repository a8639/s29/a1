const HTTP = require('http');
const express = require('express');

//express() is the server stored in the variable app
const app = express()
const PORT = 3000;

app.use(express.json())
app.use(express.urlencoded({extended:true}))

const data = [
    {
        "userName": "johndoe",
        "passWord": "johndoe1234"   
    }
];

app.get("/home", (req, res) => {
    res.send(`Welcome to the home page`)
})
app.get("/users", (req, res) => {
    const {userName, passWord} = req.body
    console.log(req.body)

})
app.delete("/delete-user", (req, res) => {
    const {userName, passWord} = req.body
    if (userName == req.body.userName) {
        res.send(`User ${userName} has been deleted`)
    } else {
        res.send (`User does not exist`)
    }
})

app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))